import * as firebase from 'firebase';

const app = firebase.initializeApp({
  apiKey: "AIzaSyANFdcxPMRpItGqJlLM9a2GffX1TxCJESo",
  authDomain: "damtopbiler.firebaseapp.com",
  databaseURL: "https://damtopbiler.firebaseio.com",
  projectId: "damtopbiler",
  storageBucket: "damtopbiler.appspot.com",
  messagingSenderId: "716347204522",
  appId: "1:716347204522:web:93abad7d3992a6168141d3",
  measurementId: "G-JKN9TLSZXV"
});

export const state = () => ({
    data: {},
    showingCarsCounter: 0,
    showingCars: []
})

export const actions = {
  load(context) {
    app.database().ref().once("value", function (snapshot) {
      const data = {};
      snapshot.forEach(function (child) {
        data[child.key] = child.val();
      });
      context.commit('loadData', data);
      context.commit('changeShowingCars');
    });
  },
  changeShowingCars(context) {
    context.commit('changeShowingCars');
  },
  sortCars(context, data) {
    context.commit("updateCarList", data);
  },
}

export const mutations = {
  loadData(state, data) {
    state.data = data.cars.sort(function(x, y){ return (x.isNew === y.isNew)? 0 : x.isNew? -1 : 1;}).filter((el) => {
      return el.pictureUrls && el.pictureUrls.length > 0;
    });
  },
  updateCarList(state, data) {
    if(data === "Value1") {
      state.data= state.data.sort(function(a,b){
        const a1 = a.price.replace(/\D/g,'');
        const b1 = b.price.replace(/\D/g,'');
        return parseInt(b1) - parseInt(a1);
      });
    } else if(data === "Value2") {
      state.data= state.data.sort(function(a,b){
        const a1 = a.price.replace(/\D/g,'');
        const b1 = b.price.replace(/\D/g,'');
        return parseInt(a1) - parseInt(b1);
      });
    } else if(data === "Value3") {
      state.data= state.data.sort(function(a,b){
        const a1 = a.km.replace(/\D/g,'');
        const b1 = b.km.replace(/\D/g,'');
        return parseInt(b1) - parseInt(a1);
      });
    } else {
      state.data= state.data.sort(function(a,b){
        const a1 = a.km.replace(/\D/g,'');
        const b1 = b.km.replace(/\D/g,'');
        return parseInt(a1) - parseInt(b1);
      });
    }
  },
  changeShowingCars(state) {
    if (state.data) {
      if((state.showingCarsCounter + 3) > state.data.length) {
        state.showingCarsCounter = 0;
      } else {
        state.showingCarsCounter += 3;
      }
      state.showingCars = state.data.slice(state.showingCarsCounter, (state.showingCarsCounter + 3));
    }
  },
  sortCars(state, sortOption) {
    state.data = state.data.sort((a,b) => {
      const aD = a.price.replace(/\D/g,'');
      const bD = b.price.replace(/\D/g,'');
      return a > b;
    });

  }
}

export const getters = {
  getNumberOfCars: (state) => {
    if(state.data.cars) {
      return state.data.cars.length;
    }
    return 0;
  }
}
